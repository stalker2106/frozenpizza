# README #

This game requires MonoGame Framework installed.

It has been compiled and tested on Windows 10 x64 Pro, and MacOS X 10.11 El Capitan.

### What to do next [UPDATELOG]###
- Add keybinds
- Build inventory system (omg)
- Fix loads of bugs.
- Enable 'create server'
- Add Epouvantail (wut?)

### How do I get set up? ###

1. Clone the repository
2. Open FrozenPizza.sln
3. Compile & Run
4. ??????
5. Enjoy

### Contribution guidelines ###

Thanks to the editors of Tiled, the map editor this project relies on.

Thanks to the author of TiledSharp, the map parser this project uses actively.

Thanks to the MonoGame project community and devs, as without them there would be nothing.
